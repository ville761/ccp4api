create table USER (
       ID bigint auto_increment,
       NAME varchar(100) not null,
       EMAIL varchar(100) not null,
       PASSWORD varchar(100) not null,
       ROLES varchar(100) not null
);

create table JOB (
       ID bigint auto_increment,
       USERID bigint not null,
       TYPE varchar(100) not null,
       STATUS varchar(100) not null
);
