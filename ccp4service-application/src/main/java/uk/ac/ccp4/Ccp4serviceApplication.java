package uk.ac.ccp4;

import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.skife.jdbi.v2.DBI;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import uk.ac.ccp4.db.UserDAO;
import uk.ac.ccp4.auth.CCP4Authenticator;
import uk.ac.ccp4.auth.CCP4Authorizer;
import uk.ac.ccp4.core.User;
import uk.ac.ccp4.db.JobDAO;
import uk.ac.ccp4.resources.JobResource;
import uk.ac.ccp4.resources.UserResource;

public class Ccp4serviceApplication extends Application<Ccp4serviceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new Ccp4serviceApplication().run(args);
    }

    @Override
    public String getName() {
        return "ccp4service";
    }

    @Override
    public void initialize(final Bootstrap<Ccp4serviceConfiguration> bootstrap) {
    	bootstrap.addBundle(new MultiPartBundle());
    	bootstrap.addBundle(new DBIExceptionsBundle());
    	bootstrap.addBundle(new ViewBundle<Ccp4serviceConfiguration>());
    	bootstrap.addBundle(new AssetsBundle("/assets/","/"));
    }

    @Override
    public void run(final Ccp4serviceConfiguration configuration,
                    final Environment environment) {
    	final DBIFactory factory = new DBIFactory();
    	final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "h2");
    	final UserDAO userdao = jdbi.onDemand(UserDAO.class);
    	final String basedir = configuration.getBasedir();
    	environment.jersey().register(new UserResource(userdao));
    	final JobDAO jobdao = jdbi.onDemand(JobDAO.class);
    	environment.jersey().register(new JobResource(jobdao, basedir));
    	environment.jersey().register(new AuthDynamicFeature(
    			new BasicCredentialAuthFilter.Builder<User>()
    				.setAuthenticator(new CCP4Authenticator(userdao))
    				.setAuthorizer(new CCP4Authorizer())
    				.setRealm("SECRET STUFF")
    				.buildAuthFilter()));
    	environment.jersey().register(RolesAllowedDynamicFeature.class);
    	environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
    }

}
