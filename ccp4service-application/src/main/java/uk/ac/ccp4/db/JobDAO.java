package uk.ac.ccp4.db;

import java.util.List;
import java.util.Optional;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.customizers.SingleValueResult;

import uk.ac.ccp4.core.Job;
import uk.ac.ccp4.core.mapper.JobMapper;

@RegisterMapper(JobMapper.class)
public interface JobDAO {

    @SqlQuery("select * from JOB")
    List<Job> getAll();

    @SqlQuery("select * from JOB where ID = :id")
    @SingleValueResult(Job.class)
    Optional<Job> findById(@Bind("id") long id);

    @SqlQuery("select * from JOB where USERID = :userId")
    List<Job> findByUser(@Bind("userId") long userId);

    @SqlUpdate("delete from JOB where ID = :id")
    int deleteById(@Bind("id") long id);

    @SqlUpdate("update into JOB set TYPE = :type where ID = :id")
    int updateType(@BindBean Job job);

    @SqlUpdate("update into JOB set USERID = :userId where ID = :id")
    int updateUser(@BindBean Job job);

    @SqlUpdate("update into JOB set STATUS = :status where ID = :id")
    int updateStatus(@BindBean Job job);

    @SqlUpdate("insert into JOB (USERID, TYPE, STATUS) values (:userId, :type, :status)")
    @GetGeneratedKeys
    long insert(@BindBean Job job);
}
