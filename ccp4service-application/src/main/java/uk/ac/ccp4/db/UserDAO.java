package uk.ac.ccp4.db;

import java.util.List;
import java.util.Optional;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.customizers.SingleValueResult;

import uk.ac.ccp4.core.User;
import uk.ac.ccp4.core.mapper.UserMapper;

@RegisterMapper(UserMapper.class)
public interface UserDAO {

    @SqlQuery("select * from USER")
    List<User> findAll();

    @SingleValueResult
    @SqlQuery("select * from USER where ID = :id")
    Optional<User> findById(@Bind("id") long id);

    @SingleValueResult
    @SqlQuery("select * from USER where EMAIL = :email")
    Optional<User> findByEmail(@Bind("email") String email);

    @SqlUpdate("delete from USER where ID = :id")
    int deleteById(@Bind("id") long id);

    @SqlUpdate("update into USER set NAME = :name where ID = :id")
    int updateName(@BindBean User user);

    @SqlUpdate("update into USER set PASSWORD = :password where ID = :id")
    int updatePassword(@BindBean User user);

    @SqlUpdate("update into USER set ROLES = :roles where ID = :id")
    int updateRoles(@BindBean User user);

    @SqlUpdate("insert into USER (NAME, EMAIL, PASSWORD, ROLES) values (:name, :email, :password, :roles)")
    @GetGeneratedKeys
    long insert(@BindBean User user);
}
