package uk.ac.ccp4.auth;

// Dropwizard 1.0 uses java.util.Optional instead of Optional from Guava
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import uk.ac.ccp4.core.User;
import uk.ac.ccp4.db.UserDAO;

public class CCP4Authenticator implements Authenticator<BasicCredentials, User> {
    private Logger logger = LoggerFactory.getLogger("uk.ac.ccp4.resources.CCP4Authenticator");
	
	// use email for login
	private UserDAO userdao;
	
	public CCP4Authenticator(UserDAO userdao) {
		this.userdao = userdao;
	}

	@Override
	public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {
		User user = userdao.findByEmail(credentials.getUsername()).orElse(null);
		if(user != null) {
			if(user.getPassword().equals(credentials.getPassword()) && user.getEmail().equals(credentials.getUsername())) {
				return Optional.of(user);
			}
		}
		return Optional.empty();
	}

}
