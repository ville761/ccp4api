package uk.ac.ccp4.auth;

import io.dropwizard.auth.Authorizer;
import uk.ac.ccp4.core.User;

public class CCP4Authorizer implements Authorizer<User> {

	@Override
	public boolean authorize(User user, String role) {
		return user.hasRole(role);
	}

}
