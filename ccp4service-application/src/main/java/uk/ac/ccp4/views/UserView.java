package uk.ac.ccp4.views;

import io.dropwizard.views.View;
import uk.ac.ccp4.core.User;

public class UserView extends View {
	private final User user;
	
	public enum Template {
		MUSTACHE("user.mustache");
		
		private String templateName;
		
		Template(String templateName) {
			this.templateName = templateName;
		}
		
		public String getTemplateName() {
			return templateName;
		}
	}
	
	public UserView(UserView.Template template, User user) {
		super(template.getTemplateName());
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}

}
