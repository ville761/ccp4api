package uk.ac.ccp4.core.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import uk.ac.ccp4.core.Job;
import uk.ac.ccp4.core.Job.Status;

public class JobMapper implements ResultSetMapper<Job> {
	public Job map(int index, ResultSet resultSet, StatementContext statementContext) throws SQLException {
		return new Job()
				.setId(resultSet.getLong("ID"))
				.setUserId(resultSet.getLong("USERID"))
				.setStatus(Status.valueOf(resultSet.getString("STATUS")))
				.setType(resultSet.getString("TYPE"));
	}

}
