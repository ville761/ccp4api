package uk.ac.ccp4.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.MoreObjects;
import com.google.common.io.Files;

// representation and domain class of a Job object

public class Job {

    private static Logger logger = LoggerFactory.getLogger("uk.ac.ccp4.core.Job");
	
	public enum Status {
		PENDING,
		RUNNING,
		STOPPING,
		FINISHED,
		ABORTED,
		FAILED
	}
	
	public enum Type {
		FREERFLAG,
		ARCIMBOLDO,
		DIMPLE
	}

	@JsonProperty
	private Long id;

	@NotNull
	@JsonProperty
	private Long userId;
	
	@NotNull
	@JsonProperty
	private String type;
	
	@JsonProperty
	private Status status;

	public Long getId() {
		return id;
	}

	public String getType() {
		return type;
	}
	
	public Status getStatus() {
		return status;
	}

	public Job setStatus(Status status) {
		this.status = status;
		return this;
	}

	public Long getUserId() {
		return userId;
	}

	public Job setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public Job setId(Long id) {
		this.id = id;
		return this;
	}

	public Job setType(String type) {
		this.type = type;
		return this;
	}

	public Job setType(Type type) {
		this.type = type.toString();
		return this;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("userid", userId)
				.add("type", type)
				.add("status", status)
				.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (status != other.status)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	
	public static String setup(Job job, String basedir) {
		Long jobid = job.getId();
		Long userid = job.getUserId();
		Path jobdir = Paths.get(basedir,userid.toString(),jobid.toString());
		try {
			jobdir.toFile().mkdirs();
		} catch (SecurityException e) {
			logger.error("Creation of the job directory "+jobdir.toString()+" was not permitted.");
		}
		// create the info file for the job
		createJson(job,jobdir);
		return jobdir.toString();
	}
	
	public static void createJson(Job job, Path jobdir) {
		ObjectMapper mapper = new ObjectMapper();
		String filePath = Paths.get(jobdir.toString(),"job.json").toString();
		try {
			mapper.writeValue(new File(filePath), job);
		} catch (IOException e) {
			logger.error("Creation of the json file "+filePath+" was not permitted.");
		}
	}

}
