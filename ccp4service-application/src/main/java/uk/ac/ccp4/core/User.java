package uk.ac.ccp4.core;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

public class User implements Principal {
	
	public enum Role {
		ADMIN,
		USER
	}

	private static String addRole(String roles, Role newrole) {
		return roles + ":" + newrole;
	}

	private static String removeRole(String roles, Role role) {
		StringBuilder sb = new StringBuilder();
		List<String> allroles = Arrays.asList(roles.split(":"));
		for(String thisrole : allroles) {
			if(!role.toString().equals(thisrole)) {
				sb.append(thisrole);
				sb.append(":");
			}
		}
		return sb.deleteCharAt(sb.length()-1).toString();
	}

    @JsonProperty
    private Long id;

	@NotNull
	@JsonProperty
    private String roles;

	@NotNull
	@JsonProperty
    private String name;

	@NotNull
	@JsonProperty
    private String email;
	
	// see the JsonIgnore and JsonProperty annotations in the setter and getter methods
	@NotNull
	private String password;
	
	public boolean hasRole(Role role) {
		List<String> rolearr = Arrays.asList(roles.split(":"));
		return rolearr.contains(role.toString());
	}

	public boolean hasRole(String role) {
		List<String> rolearr = Arrays.asList(roles.split(":"));
		return rolearr.contains(role);
	}
    
	public String addRole(Role role) {
    	roles = addRole(roles,role);
        return roles;
    }

	public String getRoles() {
		return roles;
	}

	// instead of using JsonIgnore, one could use Entity Filtering for this
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public User setPassword(String password) {
		this.password = password;
		return this;
	}

	public User setRoles(String roles) {
		this.roles = roles;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public User setEmail(String email) {
		this.email = email;
		return this;
	}

	public Long getId() {
		return id;
	}
	
	public User setId(long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}
	
	public User setName(String name) {
		this.name = name;
		return this;
	}

	public String deleteRole(Role role) {
    	roles = removeRole(roles, role);
        return roles;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (roles == null) {
			if (other.roles != null)
				return false;
		} else if (!roles.equals(other.roles))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("name", name)
				.add("email", email)
				.add("roles", roles)
				.toString();
	}

}
