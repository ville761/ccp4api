package uk.ac.ccp4.core.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import uk.ac.ccp4.core.User;

public class UserMapper implements ResultSetMapper<User> {
	public User map(int index, ResultSet resultSet, StatementContext statementContext) throws SQLException {
		return new User()
				.setId(resultSet.getLong("ID"))
				.setName(resultSet.getString("NAME"))
				.setEmail(resultSet.getString("EMAIL"))
				.setRoles(resultSet.getString("ROLES"))
				.setPassword(resultSet.getString("PASSWORD"));
	}

}
