package uk.ac.ccp4.resources;

import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.auth.Auth;
import io.dropwizard.jersey.params.LongParam;
import uk.ac.ccp4.core.User;
import uk.ac.ccp4.db.UserDAO;
import uk.ac.ccp4.views.UserView;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {
    private static Logger logger = LoggerFactory.getLogger("uk.ac.ccp4.resources.UserResource");
    
    private final UserDAO userDAO;
    
    public UserResource(UserDAO userDAO) {
    	this.userDAO = userDAO;
    }
    
    @RolesAllowed("ADMIN")
    @GET
    public List<User> getAllUsers() {
        final List<User> users = userDAO.findAll();
        if (users != null) {
            return users;
        }
        throw new WebApplicationException(Status.NOT_FOUND);
    }

    @RolesAllowed("ADMIN")
    @GET
    @Path("/{id}")
    public User getUser(@PathParam("id") LongParam id) {
    	return findSafely(id.get());
    }

    @RolesAllowed("ADMIN")
    @GET
    @Path("/{id}/view")
    @Produces(MediaType.TEXT_HTML)
    public UserView getUserView(@PathParam("id") LongParam id) {
    	return new UserView(UserView.Template.MUSTACHE, findSafely(id.get()));
    }

    @RolesAllowed("ADMIN")
    @POST
    @Path("/add")
    public User addUser(@NotNull @Valid User newuser) {
    	long id = userDAO.insert(newuser);
    	return newuser.setId(id);
    }
    
    @PermitAll
    @GET
    @Path("/mydetails")
    public User getMyDetails(@Auth User user) {
    	return findSafely(user.getId());
    }
    
    private User findSafely(long userid) {
    	return userDAO.findById(userid).orElseThrow(() -> new NotFoundException("No such user."));
    }
}