package uk.ac.ccp4.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.auth.Auth;
import io.dropwizard.jersey.params.LongParam;
import uk.ac.ccp4.core.Job;
import uk.ac.ccp4.core.User;
import uk.ac.ccp4.db.JobDAO;

@Path("/jobs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JobResource {
    private static Logger logger = LoggerFactory.getLogger("uk.ac.ccp4.resources.JobResource");
    
    @Context
    UriInfo uriInfo;
    
    private final JobDAO jobDAO;
    private String basedir;
    
    public JobResource(JobDAO jobDAO, String basedir) {
    	this.jobDAO = jobDAO;
    	this.basedir = basedir;
    }
    
    @RolesAllowed("ADMIN")
    @GET
    public List<Job> getAllByUser(@Auth User user) {
        final List<Job> jobs = jobDAO.findByUser(user.getId());
        if (jobs != null) {
            return jobs;
        }
        throw new WebApplicationException(Status.NOT_FOUND);
    }

    @RolesAllowed("ADMIN")
    @GET
    @Path("/{jobid}")
    public String getJobInfo(@Auth User user) {
    	return "viewed!";
    }

    @GET
    @Path("/{jobid}/status")
    public Response getJobStatus(@Auth User user, @PathParam("jobid") LongParam jobId) {
    	Job job = null;
    	boolean found = false;
    	try {
    		job = jobDAO.findById(jobId.get()).get();
    		if(user.getId().equals(job.getUserId())) {
    			found = true;
    		}
    	} catch(NoSuchElementException e) { // this exception is thrown by Optional.get()
    	}
    	if(!found) {
    		return Response.status(403).type("text/plain").entity("Job was not found or permission denied!").build();
    	}
    	String status = job.getStatus().toString();
		return Response.status(200).type("text/plain").entity(status).build();
    }
    
    // TODO: File validation
    @PermitAll
    @Path("/submit/freerflag")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response submit(@Auth User user, 
    		@DefaultValue("-0.05") @FormDataParam("freerfrac") float rfrac,
    		@DefaultValue("false") @FormDataParam("seed") boolean seed,
    		@DefaultValue("false") @FormDataParam("nosym") boolean nosym,
    		@DefaultValue("false") @FormDataParam("complete") boolean complete,
    		@DefaultValue("-0.05") @FormDataParam("obliquity") float obl,
    		@DefaultValue("false") @FormDataParam("unique") boolean uniq,
    		@FormDataParam("hklin") InputStream hklin, 
    		@FormDataParam("hklin") FormDataContentDisposition fd) {
    	Map<String,Object> params = new HashMap<String,Object>();
    	if(rfrac > 0.0) {
    		params.put("freerfrac", rfrac);
    	}
    	if(seed) {
    		params.put("seed", seed);
    	}
    	if(nosym) {
    		params.put("nosym", nosym);
    	}
    	if(complete) {
    		params.put("complete", complete);
    	}
    	if(obl > 0.0) {
    		params.put("obliquity", obl);
    	}
    	if(uniq) {
    		params.put("unique", uniq);
    	}
    	Job job = new Job().setType("FREERFLAG").setUserId(user.getId()).setStatus(Job.Status.PENDING);
    	long jobid = jobDAO.insert(job);
    	job.setId(jobid);
    	String jobdir = Job.setup(job, basedir);
    	String fpath = Paths.get(jobdir, fd.getFileName()).toString();
    	writeToFile(hklin, fpath);
    	writeFeed(jobdir,"freerflag",params);
    	// returns the uri for 'getJobInfo' in the header
		return Response.created(uriInfo.getBaseUriBuilder().path(JobResource.class).path(JobResource.class,"getJobInfo").build(user.getId(), jobid)).build();
    }
    
    private void writeToFile(InputStream in, String path) {
    	try(OutputStream out = new FileOutputStream(new File(path))) {
    		int read = 0;
    		byte[] bytes = new byte[1024];
    		while((read = in.read(bytes)) != -1) {
    			out.write(bytes, 0, read);
    		}
    	} catch (IOException ex) {
    		logger.error("Could not write file: "+path);
    	}
    }
    
    private void writeFeed(String jobdir, String programType, Map<String,Object> params) {
		File f = new File(jobdir,"input.txt");
		try (PrintWriter out = new PrintWriter(new FileWriter(f))) {
			out.flush();
			switch (programType) {
			case "freerflag":
				if(params.containsKey("freerfrac")) {
					Float freerfrac = (Float) params.get("freerfrac");
					out.write("FREERFRAC "+freerfrac.toString()+"\n");
				}
				if(params.containsKey("obliquity")) {
					Float obliquity = (Float) params.get("obliquity");
					out.write("TWIN "+obliquity.toString()+"\n");
				}
				if(params.containsKey("seed")) {
					Boolean seed = (Boolean) params.get("seed");
					if(seed) {
						out.write("SEED\n");
					}
				}
				if(params.containsKey("nosym")) {
					Boolean nosym = (Boolean) params.get("nosym");
					if(nosym) {
						out.write("NOSYM\n");
					}
				}
				if(params.containsKey("complete")) {
					Boolean complete = (Boolean) params.get("complete");
					if(complete) {
						out.write("COMPLETE\n");
					}
				}
				if(params.containsKey("unique")) {
					Boolean unique = (Boolean) params.get("unique");
					if(unique) {
						out.write("UNIQUE\n");
					}
				}
				out.write("END\n");
				break;
			}
		} catch (IOException e) {
    		logger.error("Could not write file: "+f.toString());
		}
	}
    	
}
